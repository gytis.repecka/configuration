# Configuration

Settings and configuration scripts for applications and tools.

## Applications

* Atom
* DataGrip
* Oh My Zsh

## Contact

E-mail at [g@gyt.is](mailto:g@gyt.is).
